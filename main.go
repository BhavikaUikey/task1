package main

import (
	"fmt"
	"main/slice"
)

func main() {

	Slice := slice.StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}

	fmt.Println("Original Slice: ", Slice)

	fmt.Print("\n")

	Slice = Slice.AddValue("Saturday")
	fmt.Println("Slice after adding a value: ", Slice)

	fmt.Print("\n")
	Slice = Slice.DeleteValue("Monday")
	fmt.Println("Slice after deleting a value: ", Slice)

	fmt.Print("\n")

	Slice = Slice.RemoveEmptyValues()
	fmt.Println("Slice after removing empty values: ", Slice)

	fmt.Print("\n")

	Slice = Slice.AddValueToIndex("day", 2)
	fmt.Println("Slice after adding a value at index: ", Slice)

	fmt.Print("\n")

	Slice = Slice.RemoveValueFromIndex(2)
	Slice = Slice.RemoveEmptyValues()
	fmt.Println("Slice after removing a value from index: ", Slice)

	fmt.Print("\n")

	Slice = Slice.SortSlice()
	fmt.Println("Slice after sorting: ", Slice)

	fmt.Print("\n")

	Slice = Slice.RemoveDuplicateValues()
	fmt.Println("Slice after removing duplicate values: ", Slice)

}
