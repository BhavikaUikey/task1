package slice

import (
	"reflect"
	"testing"
)

func TestForRemoveDuplicateValues_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	expectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
	}
	output := input.RemoveDuplicateValues()
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForRemoveDuplicateValues_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
	}
	output := input.RemoveDuplicateValues()
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForRemoveEmptyValues_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	expectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.RemoveEmptyValues()
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForRemoveEmptyValues_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.RemoveEmptyValues()
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForDeleteValue_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
	}
	expectedOutput := StringSlice{
		"Sunday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
	}
	output := input.DeleteValue("Monday")
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForDeleteValue_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.DeleteValue("Monday")
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForAddValue_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
	}
	expectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Thursday",
	}
	output := input.AddValue("Thursday")
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForAddValue_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.AddValue("Thursday")
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForRemoveValueFromIndex_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Tuesday",
		"Wednesday",
		"Saturday",
	}
	expectedOutput := StringSlice{
		"Sunday",
		"Tuesday",
		"Saturday",
	}
	output := input.RemoveValueFromIndex(2)
	output = output.RemoveEmptyValues()
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForRemoveValueFromIndex_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.RemoveValueFromIndex(1)
	output = output.RemoveEmptyValues()
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForAddValueToIndex_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
	}
	expectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Friday",
		"Wednesday",
		"",
		"",
	}
	output := input.AddValueToIndex("Friday", 3)
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForAddValueToIndex_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.AddValueToIndex("Thursday", 2)
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForSortSlice_positive(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
	}
	expectedOutput := StringSlice{
		"",
		"",
		"",
		"Monday",
		"Sunday",
		"Tuesday",
		"Wednesday",
	}
	output := input.SortSlice()
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForSortSlice_negative(t *testing.T) {
	input := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"",
		"",
		"Monday",
	}
	unexpectedOutput := StringSlice{
		"Sunday",
		"Monday",
		"",
		"Tuesday",
		"Wednesday",
		"Monday",
	}
	output := input.SortSlice()
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}
