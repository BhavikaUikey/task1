package slice

import (
	"sort"
)

type StringSlice []string

type SliceOperations interface {
	RemoveEmptyValues() StringSlice
	DeleteValue(value string) StringSlice
	AddValue(value string) StringSlice
	RemoveValueFromIndex(index int) StringSlice
	AddValueToIndex(value string, index int) StringSlice
	SortSlice() StringSlice
	RemoveDuplicateValues() StringSlice
}

func (modifiedSlice StringSlice) AddValue(value string) StringSlice {
	return append(modifiedSlice, value)
}

func (modifiedSlice StringSlice) AddValueToIndex(value string, index int) StringSlice {
	modifiedSlice[index] = value
	return modifiedSlice
}

func (modifiedSlice StringSlice) RemoveValueFromIndex(index int) StringSlice {
	modifiedSlice[index] = ""
	return modifiedSlice
}

func (modifiedSlice StringSlice) DeleteValue(value string) StringSlice {
	newSlice := StringSlice{}
	for _, element := range modifiedSlice {
		if element != value {
			newSlice = append(newSlice, element)
		}
	}
	return newSlice
}

func (modifiedSlice StringSlice) RemoveEmptyValues() StringSlice {
	newSlice := StringSlice{}
	for _, element := range modifiedSlice {
		if element != "" {
			newSlice = append(newSlice, element)
		}
	}
	return newSlice
}

func (sortedSlice StringSlice) SortSlice() StringSlice {
	sort.Strings(sortedSlice)
	return sortedSlice
}

func (modifiedSlice StringSlice) RemoveDuplicateValues() StringSlice {
	emptyMap := map[string]bool{}
	newSlice := StringSlice{}
	for _, element := range modifiedSlice {
		if !emptyMap[element] {
			newSlice = append(newSlice, element)
			emptyMap[element] = true
		}
	}
	return newSlice
}
